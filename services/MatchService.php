<?php

class MatchService
{
    public static function resultBetween($c1, $c2)
    {
        $matcher = new Matcher($c1, $c2, Data::get());
        $matcher->do();
        return $matcher->result();
    }
}